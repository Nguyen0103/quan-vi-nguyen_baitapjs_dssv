var mangNhanVien = [];

var getJSON = localStorage.getItem('mangSinhVien');
if (getJSON !== null) {
  var arrarDSNV = JSON.parse(getJSON);
  renderDSNV(mangNhanVien);
  for (var i = 0; i < arrarDSNV.length; i++) {
    var item = arrarDSNV[i]
    var nv = new Nhanvien(
      item.tknv,
      item.name,
      item.email,
      item.password,
      item.date,
      item.luongCB,
      item.chucvu,
      item.gioLam
    );
    mangNhanVien.push(nv);
  }
  renderDSNV(mangNhanVien);
};

function saveLocalStorage(dsnv) {
  var setJSON = JSON.stringify(dsnv);
  localStorage.setItem('mangSinhVien', setJSON);
}

function renderDSNV(dsnv) {
  var html = "";
  for (var i = 0; i < dsnv.length; i++) {
    var nv = dsnv[i];
    html += `<tr>
              <td>${nv.tknv}</td>
              <td>${nv.name}</td>
              <td>${nv.email}</td>
              <td>${nv.date}</td>
              <td>${nv.chucvu}</td>
              <td>${nv.tongLuong()}</td>
              <td>${nv.xepLoai()}</td>
              <td>
              <button 
                class="btn btn-danger" 
                onclick="xoaNhanVien(${nv.tknv})">Xóa</button>
              <button 
                class="btn btn-primary" 
                onclick="layThongTinNhanVienCanSua(${nv.tknv})">Sửa</button>
              </td>
            </tr>`
  }
  document.getElementById('tableDanhSach').innerHTML = html;
};

function themNV() {
  var infoNV = layThongTinTuForm();

  var valid = true;
  valid &=
    checkTKNV(infoNV.tknv, "#error_required_TKNV", "Tài Khoản ", 4, 6) &
    checkNAME(infoNV.name, "#error_required_name", "Tên nhân viên ") &
    checkEMAIL(infoNV.email, "#error_required_email", "Email ") &
    checkPASSWORD(infoNV.password, "#error_required_password", "Mật khẩu ") &
    checkDate(infoNV.date, "#error_required_date", "Ngày vào làm ") &
    checkSALARY(infoNV.luongCB, "#error_required_luongCB", " Lương cơ bản", 1000000, 20000000) &
    checkOFFICE(infoNV.chucvu, "#tbChucVu", "Chức vụ ") &
    checkWORKTIME(infoNV.gioLam, "#error_required_gioLam", " Giờ làm ", 80, 200);

  if (!valid) {
    return;
  }

  mangNhanVien.push(infoNV);
  renderDSNV(mangNhanVien);
  saveLocalStorage(mangNhanVien);
};

function xoaNhanVien(tknv) {
  var viTri = findI(mangNhanVien, tknv);
  if (viTri !== -1) {
    //xóa
    mangNhanVien.splice(viTri, 1);
    //render
    renderDSNV(mangNhanVien);
    //lưu
    saveLocalStorage(mangNhanVien);
  }
};

document.getElementById('btnCapNhat').onclick = function update() {
  document.getElementById('tknv').disabled = false;
  var nv = layThongTinTuForm();
  var index = findI(mangNhanVien, nv.tknv);

  var valid = true;
  valid &=
    checkNAME(nv.name, "#error_required_name", "Tên nhân viên ") &
    checkEMAIL(nv.email, "#error_required_email", "Email ") &
    checkPASSWORD(nv.password, "#error_required_password", "Mật khẩu ") &
    checkDate(nv.date, "#error_required_date", "Ngày vào làm ") &
    checkSALARY(nv.luongCB, "#error_required_luongCB", " Lương cơ bản", 1000000, 20000000) &
    checkOFFICE(nv.chucvu, "#tbChucVu", "Chức vụ ") &
    checkWORKTIME(nv.gioLam, "#error_required_gioLam", " Giờ làm ", 80, 200);

  if (!valid) {
    return;
  }

  if (index !== -1) {
    mangNhanVien[index] = nv;
  }

  renderDSNV(mangNhanVien);
  saveLocalStorage(mangNhanVien);
}














