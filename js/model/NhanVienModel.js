var Nhanvien = function (_tknv, _name, _email, _password, _date, _luongCB, _chucvu, _gioLam) {
    this.tknv = _tknv;
    this.name = _name;
    this.email = _email;
    this.password = _password;
    this.date = _date;
    this.luongCB = _luongCB;
    this.chucvu = _chucvu;
    this.gioLam = _gioLam;
    this.tongLuong = function () {
        var tongLuong
        if (this.chucvu == 'Giám đốc') {
            return tongLuong = this.luongCB * 3;
        }
        else if (this.chucvu == 'Trưởng phòng') {
            return tongLuong = this.luongCB * 2;
        }
        else if (this.chucvu == 'Nhân viên') {
            return tongLuong = this.luongCB;
        }
    };
    this.xepLoai = function () {
        if (this.gioLam >= 192) {
            return "Nhân viên xuất sắc"
        }
        if (this.gioLam >= 176) {
            return "Nhân viên xuất giỏi"
        }
        if (this.gioLam >= 160) {
            return "Nhân viên khá"
        }
        if (this.gioLam < 160) {
            return "Nhân viên trung bình"
        }
    };
}