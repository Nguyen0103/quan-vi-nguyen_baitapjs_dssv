function findI(mangNhanVien, tknv) {
    return mangNhanVien.findIndex(function (item) {
        return item.tknv == tknv;
    });
};

var layThongTinTuForm = function () {
    var giatri = document.getElementById('chucvu');
    console.log('giatri: ', giatri);

    var tknv = document.getElementById('tknv').value;
    var name = document.getElementById('name').value;
    var email = document.getElementById('email').value;
    var password = document.getElementById('password').value;
    var date = document.getElementById('datepicker').value;
    var luongCB = document.getElementById('luongCB').value;
    var chucvu = giatri.options[giatri.selectedIndex].text;
    var gioLam = document.getElementById('gioLam').value;

    // console.log({ tknv, name, email, password, date, luongCB, chucvu, gioLam })

    var nv = new Nhanvien(tknv, name, email, password, date, luongCB, chucvu, gioLam);
    return nv;
};

var hienThiThongTinLenForm = function (nv) {
    var giatri = document.getElementById('chucvu');

    document.getElementById('tknv').value = nv.tknv;
    document.getElementById('name').value = nv.name;
    document.getElementById('email').value = nv.email;
    document.getElementById('password').value = nv.password;
    document.getElementById('datepicker').value = nv.date;
    document.getElementById('luongCB').value = nv.luongCB;
    giatri.options[giatri.selectedIndex].text = nv.chucvu;
    document.getElementById('gioLam').value = nv.gioLam;
};

var layThongTinNhanVienCanSua = function (tknv) {
    var viTri = findI(mangNhanVien, tknv);

    if (viTri == -1) {
        return;
    }
    var Nhanvien = mangNhanVien[viTri];
    hienThiThongTinLenForm(Nhanvien);

    document.getElementById('btnThem').click();
    document.getElementById('tknv').disabled = true;
}