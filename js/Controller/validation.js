function checkTKNV(value, error, name, minLength, maxLength) {
    if (value.length == 0) {
        document.querySelector(error).innerHTML = name + " không được để trống ! "
        return false;
    }
    if (value.length < minLength || value.length > maxLength) {
        document.querySelector(error).innerHTML = name + " từ " + minLength + " đến " + maxLength + " ký tự ";
        return false;
    }
    document.querySelector(error).innerHTML = "";
    return true;
}

function checkNAME(value, error, name) {
    if (value.length == 0) {
        document.querySelector(error).innerHTML = name + " không được để trống ! "
        return false;
    }
    var regexLetter = /^[A-Za-z]+$/;
    if (regexLetter.test(value)) {
        document.querySelector(error).innerHTML = "";
        return true;
    }
    document.querySelector(error).innerHTML = name + " phải là chữ cái !"
    return false;

}

function checkEMAIL(value, error, name) {
    if (value == "") {
        document.querySelector(error).innerHTML = name + " không được để trống ! "
        return false;
    }
    var regexEmail = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (regexEmail.test(value)) {
        document.querySelector(error).innerHTML = "";
        return true;
    }
    document.querySelector(error).innerHTML = name + "Không đúng định dạng";
    return false;
}

function checkPASSWORD(value, error, name) {
    if (value.length == 0) {
        document.querySelector(error).innerHTML = name + " không được để trống ! ";
        return false;
    }
    var regexPassword = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{6,10}$/
    if (regexPassword.test(value)) {
        document.querySelector(error).innerHTML = "";
        return true;
    }
    document.querySelector(error).innerHTML = name + " chưa đúng định dạng ít nhất 1 kí tự số , 1 kí tự in hoa , 1 ký tự đặc biệt ! "
}

function checkDate(value, error, name) {
    if (value == "") {
        document.querySelector(error).innerHTML = name + " không được để trống ! "
        return false;
    }
    var regexDate = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
    if (regexDate.test(value)) {
        document.querySelector(error).innerHTML = "";
        return true;
    }
    document.querySelector(error).innerHTML = name + "Chưa đúng định dạng mm/dd/yy "
    return false;
}

function checkSALARY(value, error, name, minValue, maxValue) {
    if (value == "") {
        document.querySelector(error).innerHTML = name + " không được để trống ! "
        return false;
    }

    if (Number(value < minValue) || Number(value > maxValue)) {
        document.querySelector(error).innerHTML =
            name + " có giá trị từ " + minValue + " đến " + maxValue;
        return false;
    }

    document.querySelector(error).innerHTML = "";
    return true;
}

function checkOFFICE(value, error, name) {
    if (value == "") {
        document.querySelector(error).innerHTML = name + " không được để trống ! ";
        return false;
    }
    document.querySelector(error).innerHTML = "";
    return true;
}

function checkWORKTIME(value, error, name, minValue, maxValue) {
    if (value == "") {
        document.querySelector(error).innerHTML = name + " không được để trống ! ";
        return false;
    }
    if (Number(value < minValue) || Number(value > maxValue)) {
        document.querySelector(error).innerHTML =
            name + " trong 1 tháng phải từ " + minValue + " đến " + maxValue;
        return false;
    }
    document.querySelector(error).innerHTML = "";
    return true;
}

